# hostman

A free-as-in-beer host file generator designed to be paired with DNSMASQ and cron for a similar effect to pihole

## ARCHIVED

I switched to using AdguardHome instead of rolling my own.  Turns out it was more along the lines of what I wanted anyway...

Feel free to use and abuse however, but there will likely never be updates.