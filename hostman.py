#!/usr/bin/env python3

# "THE BEER-WARE LICENSE" (Revision 42): <zzzb@vivaldi.net> wrote this file. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return. - Robert Dash

#
# HostMan - Python
# initial reference implementation
#

#
# imports/globals
#

#imports
import sys
import requests
from pathlib import Path

#globals
database = None

#
# helper functions
#

def printUsage():
	print("usage:", sys.argv[0], "[command] <arguments>")
	print("commands available:")
	print("\tupdate - rebuild database with current sources")
	print("\twhitelist add|del|list - manipulate user-defined whitelist database")
	print("\tblacklist add|del|list - manipulate user-defined blacklist database")
	print("\tcompile - generate hosts file from current database") #TODO
	print("\tupgrade - equivalent to", sys.argv[0], "update;", sys.argv[0], "compile") #TODO
	print("\thelp [command] - get help for given command") #TODO

def openFile(filename:str, mode:str = "r", fail:bool = False):
	try:
		return open(filename, mode)
	except:
		if fail:
			print("creating config file", filename + ", please configure it before retrying.")
			exit(1)
		else:
			Path(filename).touch()
			return open(filename, mode)

def readSourceLists():
	f_black = openFile("sources.blacklist")
	f_white = openFile("sources.whitelist")
	ret = (f_black.read().splitlines(), f_white.read().splitlines())
	f_black.close()
	f_white.close()
	return ret
	
def downloadBlacklist(src):
	req = requests.get(src)
	if req.text.strip()[0] == "<":
		return {}
	stringEntries = req.text.splitlines()
	entries = []
	for entry in stringEntries:
		entry = entry.strip()
		if len(entry) == 0 or entry[0] == "#":
			continue
		entry = entry.split()
		entry = (entry[1], entry[0])
		entries.append(entry)
	return entries
	
def downloadWhitelist(src):
	reg = requests.get(src)
	if req.text.strip()[0] == "<":
		return []
	return req.text.splitlines()
	
def saveDatabase(db):
	global database
	database = db
	f = openFile("hosts.unfiltered", "w")
	for k in db.items():
		f.write(k[1] + " " + k[0] + "\n")
	f.close()
		
def loadDatabase():
	global database
	if database != None:
		return database
	db = {}
	f = openFile("hosts.unfiltered")
	for k in f.read().splitlines():
		k = k.split()
		db[k[1]] = k[0]
	f.close()
	return db
	
def loadFilterWhitelist():
	f = openFile("filter.whitelist")
	ret = set(f.read().splitlines())
	f.close()
	return ret
	
def saveFilterWhitelist(whitelist):
	f = openFile("filter.whitelist", "w")
	for k in whitelist:
		f.write(k + "\n")
	f.close()
	
def loadFilterBlacklist():
	db = {}
	f = openFile("filter.blacklist")
	for k in f.read().splitlines():
		k = k.split()
		db[k[1]] = k[0]
	f.close()
	return db
	
def saveFilterBlacklist(blacklist):
	f = openFile("filter.blacklist", "w")
	for k in blacklist.items():
		f.write(k[1] + " " + k[0] + "\n")
	f.close()
	
def saveHosts(db):
	f = openFile("hosts", "w")
	f.write("#compiled by hostman\n\n")
	for k in db.items():
		f.write(k[1] + " " + k[0] + "\n")
	f.close()
	

#
# primary functions
#

def update(args):
	#args are ignored for this command
	sources_black, sources_white = readSourceLists()
	db = {}
	#generate database from blacklists
	print("DOWNLOADING BLACKLISTS...")
	for src in sources_black:
		print("downloading", src + "...", end=" ", flush=True)
		entries = downloadBlacklist(src)
		for entry in entries:
			db[entry[0]] = entry[1]
		if len(entries) != 0:
			print("done.")
		else:
			print("failed!")
	#apply whitelists
	print("DOWNLOADING WHITELISTS...")
	for src in sources_white:
		print("downloading", src + "...", end=" ")
		entries = downloadWhitelist(src)
		for entry in entries:
			if entry in db:
				del db[entry]
		if len(entries) != 0:
			print("done.")
		else:
			print("failed!")
	#save to database file
	saveDatabase(db)
	
def whitelist(args):
	if len(args) < 1:
		helpme(["whitelist"])
		exit(-1)
	elif args[0] == "add":
		whitelist_add(args[1:])
	elif args[0] == "del":
		whitelist_del(args[1:])
	elif args[0] == "list":
		whitelist_list()
	else:
		helpme(["whitelist"])
		exit(-1)
		
def whitelist_add(args):
	if len(args) != 1:
		helpme(["whitelist"])
		exit(-1)
	white = loadFilterWhitelist()
	if args[0] in white:
		print("NOTE: filter was already present in whitelist!")
	white.add(args[0])
	saveFilterWhitelist(white)
	
def whitelist_del(args):
	if len(args) != 1:
		helpme(["whitelist"])
		exit(-1)
	white = loadFilterWhitelist()
	if args[0] not in white:
		print("NOTE: filter was not present in whitelist!")
	white.discard(args[0])
	saveFilterWhitelist(white)
	
def whitelist_list():
	white = loadFilterWhitelist()
	for term in white:
		print(term)
		
def blacklist(args):
	if len(args) < 1:
		helpme(["blacklist"])
		exit(-1)
	elif args[0] == "add":
		blacklist_add(args[1:])
	elif args[0] == "del":
		blacklist_del(args[1:])
	elif args[0] == "list":
		blacklist_list()
	else:
		helpme(["blacklist"])
		exit(-1)
		
def blacklist_add(args):
	if len(args) != 2:
		helpme(["blacklist"])
		exit(-1)
	black = loadFilterBlacklist()
	if args[0] in black:
		print("NOTE: filter was present in blacklist. overwriting...")
	black[args[0]] = args[1]
	saveFilterBlacklist(black)
	
def blacklist_del(args):
	if len(args) != 1:
		helpme(["blacklist"])
		exit(-1)
	black = loadFilterBlacklist()
	if args[0] not in black:
		print("NOTE: filter was not present in blacklist!")
	else:
		del black[args[0]]
	saveFilterBlacklist(black)
	
def blacklist_list():
	black = loadFilterBlacklist()
	for term in black.items():
		print(term[0], "->", term[1])
		
def comp(args):
	#args ignored
	db = loadDatabase()
	black = loadFilterBlacklist()
	white = loadFilterWhitelist()
	print("COMPILING HOSTS FILE...")
	for it in black.items():
		db[it[0]] = it[1]
	for it in white:
		if it in db:
			del db[it]
	saveHosts(db)
	
def helpme(args):
	if len(args) == 0:
		helpme(["help"])
	elif args[0] == "update":
		print("usage:", sys.argv[0], "update")
		print("builds an unfiltered hosts file as hosts.unfiltered")
		print("this hosts file does not have the user-defined blacklist/whitelists applied")
	elif args[0] == "whitelist":
		print("usage:", sys.argv[0], "whitelist add [url]")
		print("\tremoves the given url from the final hosts file")
		print("usage:", sys.argv[0], "whitelist del [url]")
		print("\tstops the given url from being removed from the final hosts file")
		print("usage:", sys.argv[0], "whitelist list")
		print("\tlists all urls currently being filtered out of final hosts file")
	elif args[0] == "blacklist":
		print("usage:", sys.argv[0], "blacklist add [url] [target]")
		print("\tadds the given url to the hosts file, and directs it to IP address target")
		print("usage:", sys.argv[0], "blacklist del [url]")
		print("\tstops adding the given url to the hosts file, may still be added by source hosts")
		print("usage:", sys.argv[0], "blacklist list")
		print("\tlists all urls being added to unfiltered hosts file and their redirected target IP addresses")
	elif args[0] == "compile":
		print("usage:", sys.argv[0], "compile")
		print("applies user-defined blacklist and whitelist data to hosts.unfiltered")
		print("saves result as hosts")
	elif args[0] == "upgrade":
		print("usage:", sys.argv[0], "upgrade")
		print("performs the actions of update and compile")
		print("updates the hosts file against the newest releases of the source list")
	elif args[0] == "help":
		print("usage:", sys.argv[0], "help [command]")
		print("I think you can figure this one out.")
	else:
		print("not a valid help topic")

#
# launcher
#

argc = len(sys.argv)
if argc < 2:
	printUsage()
	exit(1)
#start main logic
com = sys.argv[1]
rem = sys.argv[2:]
if com == "update":
	update(rem)
elif com == "whitelist":
	whitelist(rem)
elif com == "blacklist":
	blacklist(rem)
elif com == "compile":
	comp(rem)
elif com == "upgrade":
	update(rem)
	comp(rem)
elif com == "help":
	helpme(rem)
else:
	printUsage()
